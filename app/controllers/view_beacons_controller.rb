require 'csv'

class ViewBeaconsController < ApplicationController
  def show
    if params[:format].blank?
      @switch = Switch.where(on: 1)
      pp params
    else
      a={}
      on = Switch.find_by_id(params[:format])
      @packets = Packet.where(["created_at >= ? and created_at <= ?", on.created_at, on.created_at+30])

      # synapsensorごとのパケット内訳
      @packets.each do |e|
        a[e.advdata.split(":").last]=0 unless a.has_key?(e.advdata.split(":").last)
        a[e.advdata.split(":").last]+=1
      end
      pp a
      #return render text: get_csv_str(@packets)
      #csv出力
      return render csv: @packets
    end
  end

  def get_csv_str( packets )
      result = "\"id\",\"bdaddress\",\"deviceaddress\",\"rssi\",\"created_at\"\n"
      packets.each do |e|
        result += "#{e.id},\"#{e.bdaddress}\",\"#{e.advdata}\",#{e.rssi},\"#{e.created_at}\"\n"
      end
      result
  end
end
