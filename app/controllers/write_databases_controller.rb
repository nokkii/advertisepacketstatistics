class WriteDatabasesController < ApplicationController

  skip_before_filter :verify_authenticity_token, only: [:create]
  def create
    if @switch = Switch.last
      @switch.on == 1 ? on = true : on = false
    end
    if on
      packet = Packet.create do |s|
        s.sequence_no = params["SequenceNo"]
        s.bdaddress = params["BdAddress"]
        s.advdata = params["DeviceAddress"]
        s.created_at = params["DateTime"]
        s.rssi = params["Rssi"]
      end
    end
    render :text => "OK", :status => 200
  end

  def switch
    @packet = Packet.last
    if @switch = Switch.last
      @switch.on == 1 ? on = true : on = false
    end
    if params[:switch].present?
      @switch = Switch.create do |s|
        if @switch.present?
          s.on = @switch.on == 1 ? 0 : 1
        else
          s.on = 1
        end
          s.label = params[:label]
      end
    end
  end
end
