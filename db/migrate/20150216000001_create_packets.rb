class CreatePackets < ActiveRecord::Migration
  def change
    create_table :packets do |t|
      t.integer :sequence_no, :default => 0
      t.text :bdaddress
      t.text :advdata
      t.integer :rssi, :default => 0
      t.timestamps
    end
    add_index :packets, :id
    add_index :packets, :created_at
  end
end
