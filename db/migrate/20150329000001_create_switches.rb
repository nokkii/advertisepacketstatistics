class CreateSwitches < ActiveRecord::Migration
  def change
    create_table :switches do |t|
      t.integer :on, default: 0
      t.string  :label, default: ""
      t.timestamps
    end
    add_index :switches, :id
  end
end
