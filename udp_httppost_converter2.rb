$: << File.expand_path('../', __FILE__)

require 'eventmachine'
require 'json'
require 'sqlite3'

class UDPServer < EM::Connection
  @@port        = 12222
  @@host        = '0.0.0.0'
  @@timer       = Time.now
  @@record_flag = false
  @@db = SQLite3::Database.new("db/development.sqlite3")

  def get_record_flag
    switch = @@db.execute('select * from switches order by id desc limit 1')
    if [] != switch
      0 != switch.first[1] ? true : false
    else
      false
    end
  end

  def receive_data data
    if @@record_flag
      json_hash = JSON.parse(data)
      new_item = [
        nil,
        json_hash["SequenceNo"],
        json_hash["BdAddress"],
        json_hash["DeviceAddress"],
        json_hash["Rssi"],
        json_hash["DateTime"],
        json_hash["DateTime"]]
      sql = "insert into packets values (?,?,?,?,?,?,?)"
      @@db.execute(sql, new_item)
    end

    if @@timer + 2 < Time.now
      @@record_flag = get_record_flag
      @@timer = Time.now
    end
  rescue
  end

  def self.run
#    Process.daemon
    EM::run do
      EM::open_datagram_socket(@@host, @@port, self)
    end
  end
end

if __FILE__ == $0
  UDPServer.run
end
