# coding: utf-8

$: << File.expand_path('../', __FILE__)

require 'socket'
require 'eventmachine'
require 'net/http'
require 'uri'
require 'json'
require 'logger'

class UDPServer < EM::Connection

  @@port = 12222
  @@host = '0.0.0.0'

  @@log = Logger.new("log/convert.log")

  def receive_data data
    port, host = Socket.unpack_sockaddr_in get_peername
    @@log.debug(data)

    send_datagram(data, host, port)
    Net::HTTP.post_form(URI.parse('http://localhost:3000/write_database'),
                        JSON.parse(data))
  end

  def self.run
    Process.daemon
    EM::run do
      EM::open_datagram_socket(@@host, @@port, self)
    end
  end
end

if __FILE__ == $0
  UDPServer.run
end
